import React from 'react'
import {createAppContainer,createSwitchNavigator} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer'
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {Dimensions} from 'react-native'
import {ProfileScreen,PaymentScreen,MessageScreen,LanguageScreen,AboutScreen,ContactScreen} from './screens'
import SideBar from './Components/SideBar'
import Icon from 'react-native-vector-icons/Ionicons'
// import Home from './Components/modules/Home'




// const homeStack = createStackNavigator(
//   {
     
//     home: {screen: Home,
//     },
    
    
    
//   },
//   {},
// );

// const tabNav = createBottomTabNavigator(
//   {
//     Home: {
//       screen: homeStack,

//       navigationOptions: {
//         tabBarLabel: 'Home',
        
//         tabBarIcon: ({tintColor}) => (
//           <Icon name="ios-home" color={tintColor} size={25} />
//         ),
//       },
//     }})





const DrawerNavigator = createDrawerNavigator({
  ProfileScreen:{
    screen:ProfileScreen,
    navigationOptions:{
      title:"Profile",
     
      
      drawerIcon:({tintColor}) => <Icon name="md-people" size={25} color={tintColor}/>
    }
  },
  PaymentScreen:{
    screen:PaymentScreen,
    navigationOptions:{
      title:"Payment",
      
      drawerIcon:({tintColor}) => <Icon name="ios-card" size={25} color={tintColor}/>
    }
  },
  MessageScreen:{
    screen:MessageScreen,
    navigationOptions:{
      title:"Message",
      drawerIcon:({tintColor}) => <Icon name="md-albums" size={25} color={tintColor}/>
    }
  },
  LanguageScreen:{
    screen:LanguageScreen,
    navigationOptions:{
      title:"Language",
      drawerIcon:({tintColor}) => <Icon name="md-planet" size={25} color={tintColor}/>
    }
  },
  AboutScreen:{
    screen:AboutScreen,
    navigationOptions:{
      title:"About",
      drawerIcon:({tintColor}) => <Icon name="md-reverse-camera" size={25} color={tintColor}/>
    }
  },
  ContactScreen:{
    screen:ContactScreen,
    navigationOptions:{
      title:"Contact",
      drawerIcon:({tintColor}) => <Icon name="md-call" size={25} color={tintColor}/>
    }
  },

},
{
  contentComponent: props =><SideBar {...props}/>
}

)
// const appSwitch = createSwitchNavigator({
//   tabNav: {screen: tabNav},
  // Login:{screen:LoginStack},
// DrawerNavigator:{screen:DrawerNavigator}

// });
// const App = createAppContainer(appSwitch);
// export default App

export default createAppContainer(DrawerNavigator);