import React from 'react'
import {View,Text,StyleSheet,ScrollView,ImageBackground,Image} from 'react-native'
import { DrawerNavigatorItems} from 'react-navigation-drawer'

export default SideBar = props => (
    <ScrollView>
        <ImageBackground source={require("../images/345.jpg")}
        style={{width:undefined,padding:16,paddingTop:68,height:200}}>
            <View style={styles.profile}></View>
            <Text style={{color:"white",fontSize:12,fontWeight:"800",marginVertical:8}}>Welcome</Text>
            <View style={{flexDirection:"row"}}>
                <Text style={styles.followers}>734 Followers</Text>
            </View>
        </ImageBackground>
        <View style={styles.container}>
            <DrawerNavigatorItems {...props} />

        </View>
    </ScrollView>
);
const styles= StyleSheet.create({
    container:{
        flex:1,
    },
    profile:{
        width:70,
        height:70,
        borderRadius:40,
        borderWidth:3,
        borderColor:"#fff",
        
        
    },
    followers:{
        color:"rgba(255,255,255,0.8)",
        marginRight:4,
        marginBottom:20,
        
    }
})