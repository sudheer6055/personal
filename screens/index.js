import React from 'react'
import Screens from './screens'

export const ProfileScreen = ({navigation}) => <Screens navigation={navigation} name="Profile"/>
export const PaymentScreen = ({navigation}) => <Screens navigation={navigation} name="Payment"/>
export const MessageScreen = ({navigation}) => <Screens navigation={navigation} name="Message"/>
export const LanguageScreen = ({navigation}) => <Screens navigation={navigation} name="Language"/>
export const AboutScreen = ({navigation}) => <Screens navigation={navigation} name="About"/>
export const ContactScreen = ({navigation}) => <Screens navigation={navigation} name="Contact"/>