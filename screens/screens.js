import React from 'react'
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Modal,Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;
export default class Screens extends React.Component {
  

  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false
    }
  }
  openImgModal = (item) => {
    console.log(item)
    this.setState({ modalVisible: true, item: item })
  }


  render() {
    const { modalVisible } = this.state;
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity style={{ alignItems: "flex-end", margin: 16 }} onPress={() => this.openImgModal()}>
            <Icon
              style={{ color: 'black', marginRight: 8, }}
              name="md-notifications-outline"
              size={25}
            />
              
          </TouchableOpacity>
          <Modal
                    animationType="fade"
                    transparent={true}
                    style={{ flex: 1 }}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({ modalVisible: false })
                    }}>
                    <View style={styles.modalView}>
                        <View style={styles.modalItemView}>
                            <TouchableOpacity onPress={() => this.setState({ modalVisible: false })}>
                                <Icon name="ios-close" color="#000" size={30} style={{ alignSelf: 'flex-end' }} />
                            </TouchableOpacity>
                            <View>
                                <Text>hi</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
                
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.text}>{this.props.name}Screens</Text>
          </View>
        </SafeAreaView>

      </View>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  text: {
    color: "#161924",
    fontSize: 20,
    fontWeight: "500",

  },
  modalView: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    padding: 20,
    flex: 1,
    alignItems: 'center'
},
modalItemView: {
    backgroundColor: '#fff',
    width: deviceWidth * 90 / 100,
    padding: 15,
    borderRadius: 4,
    height: deviceHeight * 75 / 100,
    marginTop: deviceHeight * 10 / 100
},

})